
package org.example;

public class Main {
    public static void main(String[] args) throws RegistrationException {
        Costumer costumer = new Costumer("John", 12, 12);
        Costumer2 costumer2 = new Costumer2("Hannah", 35, 10);
        PriceSystem priceSystem = new PriceSystem();
        priceSystem.priceCounter(new Registration(costumer.getName(), costumer.getAge(), costumer.getDistance()));
        priceSystem.priceCounter(new Registration(costumer2.getName(), costumer2.getAge(), costumer2.getDistance()));

    }
}